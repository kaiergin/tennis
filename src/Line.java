public class Line {
	public double[] directionV = new double[3];
	public double[] positionV = new double[3];
	public Line(double[] direction, double[] point) {
		System.arraycopy(direction, 0, this.directionV, 0, direction.length);
		System.arraycopy(point, 0, this.positionV, 0, point.length);
		normalizeDirection();
	}
	private void normalizeDirection() {
		double length = Math.sqrt(Math.pow(directionV[0], 2)
				+ Math.pow(directionV[1], 2)
				+ Math.pow(directionV[2], 2));
		for (int i = 0; i < directionV.length; i++) {
			directionV[i] /= length;
		}
	}
}