import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Line2D;
import javax.swing.*;  

public class Gui extends Window {
	private static final long serialVersionUID = -6495683960631621137L;
	private static double viewPhi = 1, viewTheta = 1; // Variables used to change viewing angle
	static Tennis court = new Tennis();
	static boolean threadRunning = false;
	static Thread MoveTennisballs;
	static boolean freeView = true;
	
	
	public Gui() {
		initKeyBinds(getFrame());
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		
		// Generates tennis court
        Ball ball = court.getCurrentBall();
        Line[][] lines = null;
        if (freeView) {
        	lines = court.genScreen(viewPhi, viewTheta);
        } else {
        	lines = court.followBall(ball);
        }
		/* draws axes
		 * y: red
		 * x: green
		 * z: blue
		 */
        for (int x = 0; x < getWindowWidth(); ++x) {
			for (int y = 0; y < getWindowHeight(); ++y) {
				int courtInfo = court.placeCourt(lines[x][y]);
				double[] netInfo = court.placeNet(lines[x][y]);
				double ballInfo = court.placeBall(lines[x][y], ball);
				if (courtInfo == 1) {
					g2d.setColor(new Color(0, 30, 110));
					g2d.draw(new Line2D.Double(x, y, x, y));
				} else if (courtInfo == 2) {
					g2d.setColor(new Color(200, 200, 200));
					g2d.draw(new Line2D.Double(x, y, x, y));
				}
				if (netInfo != null && ballInfo != 0) {
					if (netInfo[1] < ballInfo) {
						if (netInfo[0] == 1) {
							g2d.setColor(new Color(180, 190, 230));
							g2d.draw(new Line2D.Double(x, y, x, y));
						} else {
							g2d.setColor(new Color(100, 100, 100));
							g2d.draw(new Line2D.Double(x, y, x, y));
						}
					} else {
						g2d.setColor(new Color(250, 160, 0));
						g2d.draw(new Line2D.Double(x, y, x, y));
					}
				} else if (ballInfo != 0) {
					g2d.setColor(new Color(250, 160, 0));
					g2d.draw(new Line2D.Double(x, y, x, y));
				} else if (netInfo != null){
					if (netInfo[0] == 1) {
						g2d.setColor(new Color(180, 190, 230));
						g2d.draw(new Line2D.Double(x, y, x, y));
					} else if (netInfo[0] == 2) {
						g2d.setColor(new Color(100, 100, 100));
						g2d.draw(new Line2D.Double(x, y, x, y));
					}
				}
			}
		}
		Graph.graphCounter = 0;
	}
	
	private static void initKeyBinds(JFrame f) {
		boolean onKeyRelease = false; // execute the key input while the key is held down
		
		// Start movement thread
		KeyStroke spaceKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, onKeyRelease);
		Action spaceAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				if (threadRunning) {
					MoveTennisballs.interrupt();
					threadRunning = false;
				} else {
					MoveObject move = new MoveObject(f, court);
					MoveTennisballs = new Thread(move);
					MoveTennisballs.start();
					threadRunning = true;
				}
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(spaceKeyStr, "SPACE");
		f.getRootPane().getActionMap().put("SPACE", spaceAction);
		
		// Change viewing angle
		KeyStroke upKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0, onKeyRelease);
		Action upAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				// Limits viewing angles
				if (viewPhi + .05 > Math.PI / 2) {
					return;
				}
				viewPhi += .05;
				f.repaint();
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(upKeyStr, "UP");
		f.getRootPane().getActionMap().put("UP", upAction);
		
		KeyStroke downKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0, onKeyRelease);
		Action downAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				// Limits viewing angles
				if (viewPhi - .05 < 0) {
					return;
				}
				viewPhi -= .05;
				f.repaint();
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(downKeyStr, "DOWN");
		f.getRootPane().getActionMap().put("DOWN", downAction);
		
		KeyStroke rightKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0, onKeyRelease);
		Action rightAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				viewTheta += 0.05;
				f.repaint();
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(rightKeyStr, "RIGHT");
		f.getRootPane().getActionMap().put("RIGHT", rightAction);
		
		KeyStroke leftKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0, onKeyRelease);
		Action leftAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				viewTheta -= 0.05;
				f.repaint();
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(leftKeyStr, "LEFT");
		f.getRootPane().getActionMap().put("LEFT", leftAction);
		
		// Zoom out
		KeyStroke wKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_W, 0, onKeyRelease);
		Action wAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				if (court.radius < 200) {
					court.radius += 2;
					f.repaint();
				}
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(wKeyStr, "W");
		f.getRootPane().getActionMap().put("W", wAction);
		
		// Zoom in
		KeyStroke sKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_S, 0, onKeyRelease);
		Action sAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				if (court.radius > 10) {
					court.radius -= 2;
					f.repaint();
				}
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(sKeyStr, "S");
		f.getRootPane().getActionMap().put("S", sAction);
		
		// For changing view
		KeyStroke vKeyStr = KeyStroke.getKeyStroke(KeyEvent.VK_V, 0, onKeyRelease);
		Action vAction = new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override public void actionPerformed(ActionEvent e) {
				freeView = !freeView;
				f.repaint();
			}
		};
		f.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(vKeyStr, "V");
		f.getRootPane().getActionMap().put("V", vAction);
	}
	
}
