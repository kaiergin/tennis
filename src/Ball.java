
public class Ball {
	
	double x, y, z;
	double radius;
	double dirX = 0, dirY = 0, dirZ = -1;
	double dirSize = 1;
	
	public Ball(double x, double y, double z, double radius) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.radius = radius;
	}
	
	public void addDirection(double x, double y, double z) {
		dirX = x;
		dirY = y;
		dirZ = z;
		calculateDirSize();
	}
	
	public void calculateDirSize() {
		dirSize = Math.sqrt(Math.pow(dirX, 2)
				+ Math.pow(dirY, 2) + Math.pow(dirZ, 2));
	}
}
