
// All measurements in feet

public class Tennis extends Graph {
	
	Ball[] tennisBalls;
	int track = 0;
	public Tennis() {
		generateTennisBalls();
	}
	
	// Takes a line to check if it intersects with the court
	public int placeCourt(Line check) {
		// Solve for intersection point
		double t = -1 * check.positionV[2] / check.directionV[2];
		if (t < 0) {
			return 0;
		}
		double x = (t * check.directionV[0]) + check.positionV[0];
		double y = (t * check.directionV[1]) + check.positionV[1];
		
		// Determine whether on line (currently 6 inch lines)
		double thickness = .5;
		// Edge lines
		if (Math.abs(x) <= 39 && Math.abs(x) >= 39 - thickness
				&& Math.abs(y) <= 18) {
			return 2;
		}
		if (Math.abs(y) <= 18 && Math.abs(y) >= 18 - thickness
				&& Math.abs(x) <= 39) {
			return 2;
		}
		// Singles lines
		if (Math.abs(y) <= 13.5 && Math.abs(y) >= 13.5 - thickness
				&& Math.abs(x) <= 39) {
			return 2;
		}
		// Service line
		if (Math.abs(x) <= 21 && Math.abs(x) >= 21 - thickness
				&& Math.abs(y) <= 13.5) {
			return 2;
		}
		// Center service line
		if (Math.abs(y) <= thickness / 2 && Math.abs(x) <= 21) {
			return 2;
		}
		// Determine whether on court
		if (Math.abs(x) <= 39 && Math.abs(y) <= 18) {
			return 1;
		}
		return 0;
	}
	
	// Takes a line to check if it intersects with net
	public double[] placeNet(Line check) {
		double height = 3.5;
		double t = -1 * check.positionV[0] / check.directionV[0];
		if (t < 0) {
			return null;
		}
		double y = (t * check.directionV[1]) + check.positionV[1];
		double z = (t * check.directionV[2]) + check.positionV[2];
		// Poles
		if (Math.abs(y) <= 21 && Math.abs(y) >= 20.5 && z > 0 && z < height) {
			return new double[] { 2, t };
		}
		// Net
		if (Math.abs(y) <= 21 && z > 0 && z < Math.pow(y, 2) / 882 + 3) {
			return new double[] { 1, t };
		}
		return null;
	}
	
	// Takes a line to check if it intersects with current ball
	public double placeBall(Line check, Ball tennisBall) {
		double aSquared = Math.pow(check.directionV[0], 2) + Math.pow(check.directionV[1], 2)
				+ Math.pow(check.directionV[2], 2);
		double ab = 2 * check.directionV[0] * (check.positionV[0] - tennisBall.x)
				+ 2 * check.directionV[1] * (check.positionV[1] - tennisBall.y)
				+ 2 * check.directionV[2] * (check.positionV[2] - tennisBall.z);
		double bSquared = Math.pow(check.positionV[0] - tennisBall.x, 2)
				+ Math.pow(check.positionV[1] - tennisBall.y, 2)
				+ Math.pow(check.positionV[2] - tennisBall.z, 2)
				- tennisBall.radius;
		if (Math.pow(ab, 2) - (4 * aSquared * bSquared) < 0) {
			return 0;
		}
		double t = (-ab + Math.sqrt(Math.pow(ab, 2) - (4 * aSquared * bSquared))) / (2 * aSquared);
		if (t < 0) {
			return 0;
		}
		return t;
	}
	
	
	// Needs to be rewritten
	public Line[][] followBall (Ball tennisBall) {
		
	  	// Finds point
		double l = radius; // Camera distance from ball
	  	double x = tennisBall.x - l*(tennisBall.dirX / tennisBall.dirSize);
	  	double y = tennisBall.y - l*(tennisBall.dirY / tennisBall.dirSize);
	  	double z = tennisBall.z - l*(tennisBall.dirZ / tennisBall.dirSize);
	  	
	    
	  	// Second point
	 	double s = 2; // FOV
		double xx = x - s*(tennisBall.dirX / tennisBall.dirSize);
		double yy = y - s*(tennisBall.dirY / tennisBall.dirSize);
		double zz = z - s*(tennisBall.dirZ / tennisBall.dirSize);
		
		if (z < 1) {
			z = 1;
			zz = 1;
		}
		
		double theta;
		double phi;
		
		if (z - zz == 0) {
			phi = Math.PI / 2;
		} else {
			phi = Math.atan(Math.sqrt(Math.pow(xx - x, 2)
					+ Math.pow(yy - y, 2)) / (zz - z));
		}
		if (phi < 0) {
			phi += Math.PI;
		}
		
		// * might be incorrect
		if (x - xx == 0 && y - yy == 0) {
			theta = 0;
		} else {
			if (xx - x > 0) {
				theta = Math.asin((yy - y)
						/ Math.sqrt(Math.pow(xx - x, 2) + Math.pow(yy - y, 2)));
			} else {
				theta = Math.PI - Math.asin((yy - y)
						/ Math.sqrt(Math.pow(xx - x, 2) + Math.pow(yy - y, 2)));
			}
		}
		
		
		// *
		
		// Screen
		double horzVect[] = { -1 * Math.sin( theta ), Math.cos( theta ), 0 };
		double vertVect[] = { Math.cos( theta ) * Math.cos( phi ),
				Math.sin( theta ) * Math.cos( phi ),
				-1 * Math.sin( phi ) }; 
		
		int size = 500;
		Line[][] vectors = new Line[size][size];
		for (int a = 0; a < size; a++) {
			for (int b = 0; b < size; b++) {
				// Points on screen
				double i = (double)(a - (size/2)) * horzVect[0] /size + x
						+ (double)(b - (size/2)) * vertVect[0] /size;
				double j = (double)(a - (size/2)) * horzVect[1] /size + y
						+ (double)(b - (size/2)) * vertVect[1] /size;
				double k = (double)(b - (size/2)) * vertVect[2] /size + z;
				
				double[] arg = {i, j, k};
				double[] dirVector = { i - xx, j - yy, k - zz };
				vectors[a][b] = new Line(dirVector, arg);
			}
		}
		return vectors;
	}
	
	public void generateTennisBalls() {
		tennisBalls = new Ball[71];
		// Sample ball path
		for (int i = -35; i <= 35; i++) {
			double x = i;
			double y = (double)i / 4;
			double z = (-Math.pow(i, 2) / 140) + 10;
			tennisBalls[i + 35] = new Ball(x, y, z, .5);
		}
		for (int i = 0; i < 70; i++) {
			tennisBalls[i].addDirection(tennisBalls[i + 1].x - tennisBalls[i].x,
					tennisBalls[i + 1].y - tennisBalls[i].y,
					tennisBalls[i + 1].z - tennisBalls[i].z);
		}
		return;
	}
	
	public Ball getNextBall() {
		track++;
		if (track > tennisBalls.length) {
			track = 0;
		}
		return tennisBalls[track];
	}
	
	public Ball getCurrentBall() {
		return tennisBalls[track];
	}
	
}
