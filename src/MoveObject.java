import javax.swing.JFrame;

public class MoveObject implements Runnable {

	JFrame f;
	Tennis court;
	
	public MoveObject(JFrame f, Tennis court) {
		this.f = f;
		this.court = court;
	}
	
	@Override
	public void run() {
		while (true) {
			court.track++;
			if (court.track >= court.tennisBalls.length) {
				court.track = 0;
			}
			f.repaint();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				return;
			}
		}
	}
	
}
