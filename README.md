# 3D Tennis Court

Created by Kai Ergin

An extension of the 3D graphing calculator

Controls below

![](demo.gif)

How to run

*  cd into the bin directory
*  java Window

How to control

*  spacebar - starts/stops ball
*  v - changes view to follow ball
*  arrow keys - changes viewing angle
*  w/s - zooms in/out